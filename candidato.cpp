#include <iostream>
#include "candidato.hpp"

using namespace std;

Candidato::Candidato(){
	setNome("");
	setSexo("");
	setUF("");
	setCPF("");
	setCargo("");
	setNumero("");
	setPartido("");
	setVotos_recebidos(0);
}
Candidato::~Candidato(){}

void Candidato::setCargo(string cargo){
	this->cargo = cargo;
}
string Candidato::getCargo(){
	return cargo;
}
void Candidato::setNumero(string numero){
	this->numero = numero;
}
string Candidato::getNumero(){
	return numero;
}
void Candidato::setPartido(string partido){
	this->partido = partido;
}
string Candidato::getPartido(){
	return partido;
}
void Candidato::setVotos_recebidos(int votos_recebidos){
	this->votos_recebidos = votos_recebidos;
}
int Candidato::getVotos_recebidos(){
	return votos_recebidos;
}
void Candidato::imprimeDados(){
	cout << "Candidato: " << getNome() << endl;
	cout << "Sexo: " << getSexo() << endl;
	cout << "UF: " << getUF() << endl;
	cout << "CPF: " << getCPF() << endl;
	cout << "Cargo: " << getCargo() << endl;
	cout << "Numero: " << getNumero() << endl;
	cout << "Partido: " << getPartido() << endl << endl;
}
