#ifndef ELEITOR_HPP
#define ELEITOR_HPP

#include "pessoa.hpp"

using namespace std;

class Eleitor : public Pessoa {

	private:
		string presidente;
		string governador;
		string senador;
		string estaduais;
		string distritais;
	
	public:
		Eleitor();
		~Eleitor();
		void setPresidente(string  presidente);
		string getPresidente();
		void setGovernador(string governador);
		string getGovernador();
		void setSenador(string senador);
		string  getSenador();
		void setEstaduais(string estaduais);
		string getEstaduais();
		void setDistritais(string distritais);
		string getDistritais();
		void imprimeDados();
};

#endif
