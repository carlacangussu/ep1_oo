#ifndef PESSOA_HPP
#define PESSOA_HPP

#include <string>

using namespace std;

class Pessoa {

	private:
		string nome;
		string sexo;
		string UF;
		string CPF;

	public:
		Pessoa();
		~Pessoa();
		void setNome(string nome);
		string getNome();
		void setSexo(string sexo);
		string getSexo();
		void setUF(string UF);
		string getUF();
		void setCPF(string CPF);
		string getCPF();
};

#endif
