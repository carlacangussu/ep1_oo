#include <iostream>
#include "eleitor.hpp"

using namespace std;

Eleitor::Eleitor(){
	setNome("");
	setSexo("");
	setUF("");
	setCPF("");
	setPresidente("");
	setGovernador("");
	setSenador("");
	setEstaduais("");
	setDistritais("");
}
Eleitor::~Eleitor(){}

void Eleitor::setPresidente(string presidente){
	this->presidente = presidente;
}
string Eleitor::getPresidente(){
	return presidente;
}
void Eleitor::setGovernador(string governador){
	this->governador = governador;
}
string Eleitor::getGovernador(){
	return governador;
}
void Eleitor::setSenador(string senador){
	this->senador = senador;
}
string Eleitor::getSenador(){
	return senador;
}
void Eleitor::setEstaduais(string estaduais){
	this->estaduais = estaduais;
}
string Eleitor::getEstaduais(){
	return estaduais;
}
void Eleitor::setDistritais(string distritais){
	this->distritais = distritais;
}
string Eleitor::getDistritais(){
	return distritais;
}
void Eleitor::imprimeDados(){
	cout << "Eleitor: " << getNome() << endl;
	cout << "Sexo: " << getSexo() << endl;
	cout << "UF: " << getUF() << endl;
	cout << "CPF: " << getCPF() << endl;
	cout << "Presidente: " << getPresidente() << endl;
	cout << "Governador: " << getGovernador() << endl;
	cout << "Senador: " << getSenador() << endl;
	cout << "Deputado estadual: " << getEstaduais() << endl;
	cout << "Deputado distrital: " <<getDistritais() << endl;
}
