Aluna: Carla Rocha Cangussú
Matricula: 170085023

Exercício prático 1 de Orientações a objetos 2018.2

Para compilar o seguinte programa por meio do terminal ao diretório onde os arquivo estão armazenados e utilizar g++. Caso não possua este compilador basta seguir as orientações do próprio ternimal para fazer o download.
Com o g++ devidamente instalado digite no terminal g++ main.cpp pessoa.cpp eleitor.cpp candidato.cpp
Para executar digite ./a.out

Obs: fiz algumas alterações no arquivo csv para pegar apenas as informações importantes.
