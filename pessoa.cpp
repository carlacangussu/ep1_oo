#include "pessoa.hpp"
#include <iostream>

Pessoa::Pessoa(){
	nome = "";
	sexo = "";
	UF = "";
	CPF = "";
}
Pessoa::~Pessoa(){}

void Pessoa::setNome(string nome){
	this->nome = nome;
}
string Pessoa::getNome(){
	return nome;
}
void Pessoa::setSexo(string sexo){
	this->sexo = sexo;
}
string Pessoa::getSexo(){
	return sexo;
}
void Pessoa::setUF(string UF){
	this->UF =UF;
}
string Pessoa::getUF(){
	return UF;
}
void Pessoa::setCPF(string CPF){
	this->CPF = CPF;
}
string Pessoa::getCPF(){
	return CPF;
}
