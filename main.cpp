#include <iostream>
#include <fstream>
#include <string.h>
#include "pessoa.hpp"
#include "eleitor.hpp"
#include "candidato.hpp"

using namespace std;

int num_votantes, branco, confirmar, cont = 0;
string nome, numero, presidente, governador, senador, d_estadual, d_distrital, sexo, uf, cargo, partido, cpf;

string leitura(string voto){
	if(voto == "BRANCO"){
                        branco++;}
	else{

	Candidato * candidato = new Candidato();
	delete candidato;

	ifstream ip("consulta_cand_2018_BR.csv");

			if(!ip.is_open()){
				cout << "Erro o arquivo não foi aberto" << endl;
			}
			while(ip.good()){
				getline(ip, numero, ';');
				candidato->setNumero(numero);
				getline(ip, uf, ';');
				candidato->setUF(uf);
				getline(ip, cargo, ';');
				candidato->setCargo(cargo);
				getline(ip, nome, ';');
				candidato->setNome(nome);
				getline(ip, cpf, ';');
				candidato->setCPF(cpf);
				getline(ip, partido, ';');
				candidato->setPartido(partido);
				getline(ip, sexo, '\n');	
				candidato->setSexo(sexo);
	
				if(voto == numero){
					candidato->imprimeDados();  
				}				
			}
	ip.close();
	}
}

int main(int argc, char ** argv){

	cout << "Insira o número de votantes: " << endl;
	cin >>  num_votantes;
	
	for(cont = 0; cont < num_votantes; cont++){
		Eleitor * eleitor = new Eleitor();
		delete eleitor;
		cout << "-------------------------" << endl;
		cout << "Insira seu nome: " << endl;
		cin >> nome;
		eleitor->setNome(nome);
		cout << " Insira seu sexo: " << endl;
		cin >> sexo;
		eleitor->setSexo(sexo);
		cout << " Insira seu UF: "<< endl;
		cin >> uf;
		eleitor->setUF(uf);
		cout << "Insira seu CPF: " <<endl;
		cin >> cpf;
		eleitor->setCPF(cpf);
		cout << "Insira seu voto para Presidente entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
		cin >> presidente;
		cout << "-------------------------"<< endl;
		
  		leitura(presidente);
		cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
		cin >> confirmar;
		while(confirmar == 2){
			cout << "Insira seu voto para Presidente entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
			cin >> presidente;
			cout << "-------------------------"<< endl;
			
			leitura(presidente);
			cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
			cin >> confirmar;
		}
		if(confirmar == 3){
			cout << "Votação cancelada pelo eleitor" << endl;
			break;}
		else if(confirmar == 1){
			eleitor->setPresidente(presidente);}

		cout << "Insira seu voto para Governador entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
		cin >> governador;
		cout << "-------------------------"<< endl;
		
		leitura(governador);
		cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
		cin >> confirmar;
		while(confirmar == 2){
			cout << "Insira seu voto para Governador entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
			cin >> governador;
			cout << "-------------------------"<< endl;
			
			leitura(governador);
			cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
			cin >> confirmar;
		}
		if(confirmar == 3){
			cout << "Votação cancelada pelo eleitor" << endl;
			break;}
		else if(confirmar == 1){
			eleitor->setGovernador(governador);}


		cout << "Insira seu voto para Senador entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
		cin >> senador;
		cout << "-------------------------"<< endl;

		leitura(senador);
		cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
		cin >> confirmar;
		while(confirmar == 2){
			cout << "Insira seu voto para Senador entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
			cin >> senador;
			cout << "-------------------------"<< endl;
			
			leitura(senador);
			cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
			cin >> confirmar;
		}
		if(confirmar == 3){
			cout << "Votação cancelada pelo eleitor" << endl;
			break;}	
		else if(confirmar == 1){
			eleitor->setSenador(senador);}

		cout << "Insira seu voto para Deputado distrital entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
		cin >> d_distrital;
		cout << "-------------------------"<< endl;

		leitura(d_distrital);
		cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
		cin >> confirmar;
		while(confirmar == 2){
			cout << "Insira seu voto para Deputado distrital entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
			cin >> d_distrital;
			cout << "-------------------------"<< endl;
			
			leitura(d_distrital);
			cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
			cin >> confirmar;
		}
		if(confirmar == 3){
			cout << "Votação cancelada pelo eleitor" << endl;
			break;}
		else if(confirmar == 1){
			eleitor->setDistritais(d_distrital);
		}

		cout << "Insira seu voto para Deputado estadual entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
		cin >> d_estadual;
		cout << "-------------------------"<< endl;

		leitura(d_estadual);
		cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
		cin >> confirmar;
		while(confirmar == 2){
			cout << "Insira seu voto para Deputado estadual entre aspas duplas ou a pavalra BRANCO para votar em branco: " << endl;
			cin >> d_estadual;
			cout << "-------------------------"<< endl;
			
			leitura(d_estadual);
			cout << "Para confirma digite 1, para corrigir ou cancelar passo digite 2 e para cancelar votacao digite 3: "<< endl;
			cin >> confirmar;
		}
		if(confirmar == 3){
			cout << "Votação cancelada pelo eleitor" << endl;
			break;}	
		else if(confirmar == 1){
			eleitor->setEstaduais(d_estadual);
		}			
		eleitor->imprimeDados();
	}
	return 0;}
