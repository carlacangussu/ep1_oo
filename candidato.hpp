#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include "pessoa.hpp"

using namespace std;

class Candidato : public Pessoa {

	private:
		string cargo;
		string numero;
		string partido;
		int votos_recebidos;

	public:
		Candidato();
		~Candidato();
		void recebeDados();
		void setCargo(string cargo);
		string getCargo();
		void setNumero(string numero);
		string getNumero();
		void setPartido(string partido);
		string getPartido();
		void setVotos_recebidos(int votos_recebidos);
		int getVotos_recebidos();
		void imprimeDados();
};

#endif
